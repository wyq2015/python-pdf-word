
# python-pdf-word

#### 介绍
基于python的pdf文档转换为word文档


#### 安装教程

1.  pip install pdfminer3k  如果pip 下载较慢后面跟上 -i https://pypi.tuna.tsinghua.edu.cn/simple/

#### 使用说明

Windows下

    . 修改config.cfg文件，指定存放pdf和word文件的文件夹，以及同时工作的进程数

    . 运行main.py

Linux环境下

    . 下载到本地

    . 进入项目目录，建立虚拟环境，并安装依赖

        cd pdf2word

        python3 -m venv venv 
 
        source venv/bin/activate

        pip install -r requirements.txt

    . 修改config.cfg文件，指定存放pdf和word文件的文件夹，以及同时工作的进程数

    . 执行python main.py